module gitlab.com/statehub/statehub-controller

go 1.15

require (
	github.com/google/gofuzz v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/statehub/openapi-go v0.1.2
	gitlab.com/statehub/state-controller v0.0.11
	gitlab.com/statehub/statehub-cloud v0.1.28
	golang.org/x/oauth2 v0.0.0-20210628180205-a41e5a781914 // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	k8s.io/api v0.21.2
	k8s.io/apimachinery v0.21.2
	k8s.io/client-go v0.21.2
	k8s.io/klog/v2 v2.9.0 // indirect
	sigs.k8s.io/structured-merge-diff/v4 v4.1.1 // indirect
)
