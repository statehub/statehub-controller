package main

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"

	statehub "gitlab.com/statehub/openapi-go"
	statehubv1alpha1 "gitlab.com/statehub/state-controller/api/v1alpha1"
	statehubTypes "gitlab.com/statehub/statehub-cloud/pkg/types"
)

type reconciler struct {
	log            *log.Entry
	statehubClient *statehub.APIClient
	statesClient   *rest.RESTClient
	k8sClientSet   *kubernetes.Clientset
}

const kubernetesRegionTopologyLabel string = "topology.kubernetes.io/region"

var (
	statehubNamespace = "statehub-system"
	clusterID         = os.Getenv("CLUSTER_NAME")
	statehubURL       = os.Getenv("STATEHUB_API")
	statehubToken     = os.Getenv("STATEHUB_TOKEN")
	cleanupGrace      = os.Getenv("CLEANUP_GRACE")
)

func main() {

	logger := log.WithFields(log.Fields{"main": "statehub-controller"})

	config, err := rest.InClusterConfig()
	if err != nil {
		logger.Fatalln("error getting service account credentials:", err)
	}
	statehubv1alpha1.AddToScheme(scheme.Scheme)

	statesConfig := *config
	statesConfig.ContentConfig.GroupVersion = &statehubv1alpha1.GroupVersion
	statesConfig.APIPath = "/apis"
	statesConfig.NegotiatedSerializer = serializer.NewCodecFactory(scheme.Scheme)
	statesConfig.UserAgent = rest.DefaultKubernetesUserAgent()

	statesClient, err := rest.UnversionedRESTClientFor(&statesConfig)
	if err != nil {
		logger.Fatalln("error creating states client:", err)
	}

	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		logger.Fatalln("error creating kubernetes client set:", err)
	}

	statehubConfig := statehub.NewConfiguration()
	uri, err := url.ParseRequestURI(statehubURL)
	if err != nil {
		logger.Errorln(err, "statehub uri api error")
		os.Exit(1)
	}
	statehubConfig.Host = uri.Host
	statehubConfig.Scheme = uri.Scheme
	statehubConfig.HTTPClient = http.DefaultClient
	statehubConfig.HTTPClient.Timeout = 8 * time.Second
	statehubConfig.Debug = false
	statehubTokenHeader := fmt.Sprint("Bearer ", statehubToken)
	statehubConfig.AddDefaultHeader("Authorization", statehubTokenHeader)
	statehubClient := statehub.NewAPIClient(statehubConfig)

	reconciler := reconciler{
		log:            log.WithFields(log.Fields{"statehub-controller": "reconciler"}),
		statehubClient: statehubClient,
		statesClient:   statesClient,
		k8sClientSet:   clientSet,
	}

	reconciler.reconcile(5*time.Second, reconciler.syncStates)

	logger.Info("exiting")

}

func (r *reconciler) reconcile(interval time.Duration, f func() error) {
	for range time.Tick(interval) {
		err := f()
		if err != nil {
			log.Errorln("reconciliation error: ", err)
		}
	}
}

func (r *reconciler) syncStates() error {

	r.log.Infoln("synchronizing")

	// err := updateClusterLocations(context.TODO(), r.statehubClient, r.k8sClientSet, clusterID)
	// if err != nil {
	// 	r.log.Errorf("failed to update cluster locations: %s", err)
	// }
	// r.log.Info("updated cluster locations")

	remoteStates, err := getRemoteStates(r.statehubClient)
	if err != nil {
		r.log.Errorf("failed to get remote states: %s", err)
	}
	r.log.Info("remote states:", remoteStates)

	localStates := statehubv1alpha1.StateList{}
	err = r.statesClient.Get().Resource("states").Do(context.TODO()).Into(&localStates)
	if err != nil {
		return fmt.Errorf("error getting local states: %s", err)
	}

	// log.Infof("remote states: %+v", remoteStates)
	// log.Infof("local states: %+v", localStates.Items)

	for _, remoteState := range remoteStates {
		r.log.Infof("checking if state %s should be created", remoteState.Name)
		if !isStateInStates(remoteState, localStates.Items) {
			r.log.Infof("state, %s, exists on statehub but not on the cluster, creating.", remoteState.Name)
			_, err = createState(&remoteState, r.statesClient)
			if err != nil {
				return fmt.Errorf("failed to create state:%s/%s: %s", remoteState.Namespace, remoteState.Name, err)
			}
			r.log.Infof("created state, %s", remoteState.Name)
		}
	}

	for _, localState := range localStates.Items {
		r.log.Infof("checking if state %s should be deleted", localState.Name)
		if !isStateInStates(localState, remoteStates) {
			if localState.Status.FirstMissed == "" {
				localState.Status.FirstMissed = time.Now().Format(time.RFC3339)
				updatedState, err := updateStateStatus(context.TODO(), &localState, r.statesClient)
				if err != nil {
					return fmt.Errorf("failed to update state:%s/%s: %s", localState.Namespace, localState.Name, err)
				}
				r.log.Infof("set first miss attribute: %s", updatedState.Status.FirstMissed)
			}
			firstMissed, err := time.Parse(time.RFC3339, localState.Status.FirstMissed)
			if err != nil {
				return fmt.Errorf("failed to parse first missed time from status: %s", err)
			}
			parsedCleanupGrace, err := time.ParseDuration(cleanupGrace)
			if err != nil {
				return fmt.Errorf("failed to parse cleanup grace")
			}

			graceTimeout := firstMissed.Add(parsedCleanupGrace)
			r.log.Infof("grace period will be over %s after %v", parsedCleanupGrace, firstMissed)

			r.log.Infof("checking if %s passed(now: %s)", graceTimeout, time.Now())
			if graceTimeout.Before(time.Now()) {
				r.log.Infof("state, %s, exists on cluster but not on the statehub and grace period is over, marking for deletion.", localState.Name)
				if localState.DeletionTimestamp.IsZero() {
					err := deleteState(localState, r.statesClient)
					// updatedLocalState, err := updateDeletionTimestamp(context.TODO() , &localState, r.statesClient)
					if err != nil {
						return fmt.Errorf("failed to update deletion timestamp of state:%s/%s: %s", localState.Namespace, localState.Name, err)
					}
					// r.log.Infof("marked state %s for deleteion at %s", updatedLocalState.Name, *localState.DeletionTimestamp)
					r.log.Infof("sent delete request for state %s ", *localState.DeletionTimestamp)
				} else
				{
					r.log.Infof("state %s was marked for deleteion at %s", localState.Name, *localState.GetDeletionTimestamp())
				}

			}
		}
	}

	r.log.Infoln("end of sync")
	return nil
}

func updateDeletionTimestamp(ctx context.Context, state *statehubv1alpha1.State, statesClient *rest.RESTClient) (result *statehubv1alpha1.State, err error) {

	result = &statehubv1alpha1.State{}
	now := metav1.Now()
	state.SetDeletionTimestamp(&now)
	err = statesClient.Put().
		Namespace(state.Namespace).
		Resource("states").
		Name(state.Name).
		VersionedParams(&metav1.UpdateOptions{}, scheme.ParameterCodec).
		Body(state).
		Do(ctx).Into(result)
	return
}

func updateStateStatus(ctx context.Context, state *statehubv1alpha1.State, statesClient *rest.RESTClient) (result *statehubv1alpha1.State, err error) {

	result = &statehubv1alpha1.State{}
	err = statesClient.Put().
		Namespace(state.Namespace).
		Resource("states").
		Name(state.Name).
		SubResource("status").
		VersionedParams(&metav1.UpdateOptions{}, scheme.ParameterCodec).
		Body(state).
		Do(ctx).Into(result)
	return
}

func isStateInStates(state statehubv1alpha1.State, states []statehubv1alpha1.State) bool {

	for _, listedState := range states {
		if listedState.Name == state.Name {
			return true
		}
	}

	return false
}

func deleteState(state statehubv1alpha1.State, statesClient *rest.RESTClient) error {

	result := statesClient.Delete().Resource("states").Namespace(state.Namespace).Name(state.Name).Do(context.TODO())
	if result.Error() != nil {
		return result.Error()
	}
	return nil

}

func createState(remoteState *statehubv1alpha1.State, statesClient *rest.RESTClient) (*statehubv1alpha1.State, error) {

	state := statehubv1alpha1.State{}
	err := statesClient.Post().
		Namespace(remoteState.Namespace).
		Resource("states").
		Body(remoteState).
		Do(context.TODO()).Into(&state)
	if err != nil {
		return nil, err
	}
	return &state, nil
}

func getRemoteStates(statehubClient *statehub.APIClient) ([]statehubv1alpha1.State, error) {

	remoteStates := []statehubv1alpha1.State{}
	statehubStates, _, err := statehubClient.StatesApi.StatesControllerFindManyExecute(statehubClient.StatesApi.StatesControllerFindMany(context.TODO()))
	if err != nil {
		return nil, err
	}

	for _, statehubState := range statehubStates {
		remoteStates = append(remoteStates, statehubv1alpha1.State{
			TypeMeta: metav1.TypeMeta{
				Kind:       "State",
				APIVersion: "state.statehub.io/v1alpha1",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      statehubState.Name,
				Namespace: statehubNamespace,
			},
		})

	}
	return remoteStates, nil
}

func getClusterLocations(ctx context.Context, clientSet kubernetes.Interface) (map[string]statehubv1alpha1.Location, error) {

	nodeList := &corev1.NodeList{}
	// label := fmt.Sprintf("%s=enabled", statehubTypes.StatehubStatusTopologyLabel)
	nodeList, err := clientSet.CoreV1().Nodes().List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	locations := map[string]statehubv1alpha1.Location{}

	for _, node := range nodeList.Items {
		region := node.Labels[kubernetesRegionTopologyLabel]
		provider := node.Labels[statehubTypes.StatehubProviderTopologyLabel]
		providerScope := node.Labels[statehubTypes.StatehubProviderScopeTopologyLabel]
		location := statehubv1alpha1.Location{Region: region, Provider: provider, ProviderScope: providerScope}
		locations[region] = location
	}
	//TODO: return only unique locations (Use hashset)
	return locations, nil
}

func updateClusterLocations(ctx context.Context, statehubClient *statehub.APIClient, clientSet kubernetes.Interface, clusterID string) error {
	locations, err := getClusterLocations(context.TODO(), clientSet)
	if err != nil {
		// log.Error(err, "Failed to generate cluster locations")
		return err
	}
	// log.Info("got cluster locations: %+v", locations)

	AwsLocationsDto := []statehub.ClusterLocationAwsDto{}
	AzureLocationsDto := []statehub.ClusterLocationAzureDto{}
	for _, location := range locations {
		switch location.Provider {
		case string(statehubTypes.Aws):
			providerScope := fmt.Sprintf("arn:aws:iam::%s:root", location.ProviderScope)
			// We cant have the providerScope on the node to be this
			// because the labels needs to comply with dns regex.
			dto := *statehub.NewClusterLocationAwsDto(location.Region, providerScope)
			AwsLocationsDto = append(AwsLocationsDto, dto)
		case string(statehubTypes.Azure):
			dto := *statehub.NewClusterLocationAzureDto(location.Region)
			AzureLocationsDto = append(AzureLocationsDto, dto)
		}
	}

	clusterLocationsDto := statehub.ClusterLocationsDto{
		Aws:   AwsLocationsDto,
		Azure: AzureLocationsDto,
	}

	_ = statehubClient.ClusterLocationsApi.ClusterLocationsControllerUpdateLocations(ctx, clusterID).ClusterLocationsDto(clusterLocationsDto)
	updateLocationsRequest := statehubClient.ClusterLocationsApi.ClusterLocationsControllerUpdateLocations(ctx, clusterID).ClusterLocationsDto(clusterLocationsDto)
	_, _, err = statehubClient.ClusterLocationsApi.ClusterLocationsControllerUpdateLocationsExecute(updateLocationsRequest)
	if err != nil {
		return err
	}
	return nil
}
