#!/bin/sh -x

# Assume role somehow?

HELM_URL=https://get.helm.sh/helm-v3.6.0-linux-amd64.tar.gz
HELM=linux-amd64/helm
BASE=https://helm.statehub.io
BUCKET=s3://statehub-helm
CLOUDFRONT_DIST="E103I5M2XX5VSQ"
DIR=$(mktemp -d)
CHART_PATH=$(dirname $(readlink -f $0))/charts/statehub-controller

# Get Helm
cd $DIR
wget -O helm.tgz $HELM_URL
tar xf helm.tgz
rm helm.tgz

# Create package
$HELM package $CHART_PATH

# Get existing index, and create a new one based on it with our new chart
aws s3 cp $BUCKET/index.yaml existing.yaml
$HELM repo index . --merge existing.yaml --url $BASE

# Upload stuff
aws s3 cp *tgz $BUCKET
aws s3 cp index.yaml $BUCKET

# Invalidate old index.yaml
aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_DIST --paths "/index.yaml"
